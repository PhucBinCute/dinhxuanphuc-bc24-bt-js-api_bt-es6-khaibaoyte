const Base_URL = "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products";
let idSanPhamThemGioHang = null;

let gioHang = [];

let hienThiDanhSach = () => {
  axios({
    url: Base_URL,
    method: "GET",
  })
    .then(function (res) {
      console.log("Data API", res.data);
      renderTableSanPham(res.data);
    })
    .catch(function (err) {
      console.log("no", err);
    });
};

let renderTableSanPham = (data) => {
  let contentHTML = "";
  for (let i = 0; i < data.length; i++) {
    let sanPham = data[i];
    let contentItem = `
                        <div class="product-item">
                            <div class="product-content">
                                <div class="product-image">
                                    <img src="${sanPham.img}" alt="" />
                                </div>
                            <div class="product-info">
                                <h4 class="product-name">${sanPham.name}</h4>
                                <h4 class="product-price">${sanPham.price}</h4>
                            </div>
                            <div class="btn">
                                <button type="button" onclick=themSanPhamVaoGioHang(${sanPham.id})>Add to Cart</button>
                            </div>
                            </div>
                        </div>
                        `;
    contentHTML += contentItem;
  }
  document.getElementById("product-list").innerHTML = contentHTML;
};

document.getElementById("brand").addEventListener("change", function () {
  let resultChange = document.getElementById("brand").value;
  if (resultChange === "all") {
    hienThiDanhSach();
  } else {
    locSanPhamTheoHang(resultChange);
  }
});

let locSanPhamTheoHang = (brandName) => {
  let listProductByBrand = [];

  axios({
    url: Base_URL,
    method: "GET",
  })
    .then(function (res) {
      let listProducts = res.data;
      console.log(listProducts);
      for (let i = 0; i < listProducts.length; i++) {
        if (listProducts[i].type.toLowerCase() === brandName.toLowerCase()) {
          listProductByBrand.push(listProducts[i]);
        }
      }
      renderTableSanPham(listProductByBrand);
    })
    .catch(function (err) {
      console.log("no", err);
    });
};

let themSanPhamVaoGioHang = (id) => {
  let sanPhamThem = {};
  let flag = false;
  axios({
    url: `${Base_URL}/${id}`,
    method: "GET",
  })
    .then(function (res) {
      for (let i = 0; i < gioHang.length; i++) {
        let sanPhamTrongGio = gioHang[i];
        if (+sanPhamTrongGio.sanpham.id === id) {
          gioHang[i].soluong += 1;
          flag = true;
        }
      }
      if (!flag) {
        sanPhamThem.sanpham = res.data;
        sanPhamThem.soluong = 1;
        gioHang.push(sanPhamThem);
      }

      renderCart(gioHang);
      tongTienThanhToan();
      saveCartInLocal();
    })
    .catch(function (err) {
      console.log("no", err);
    });
};

let renderCart = (data) => {
  let contentHTML = "";
  for (let i = 0; i < data.length; i++) {
    let spGioHang = data[i];
    let trContent = ` <tr>
                                <td class="td-img">
                                    <img src="${
                                      spGioHang.sanpham.img
                                    }" alt"" />  
                                </td>
                                <td> ${spGioHang.sanpham.name} </td>
                                <td> ${spGioHang.sanpham.price} </td>
                                <td> 
                                    <span>${spGioHang.soluong} </span>
                                    <button onclick=themSoLuong(${
                                      spGioHang.sanpham.id
                                    })><i class="fa-solid fa-angle-up"></i></button>
                                    <button onclick=giamSoLuong(${
                                      spGioHang.sanpham.id
                                    })><i class="fa-solid fa-angle-down"></i></button>
                                </td>
                                <td> ${
                                  spGioHang.sanpham.price * spGioHang.soluong
                                } </td>
                                <td>
                                    <button onclick="deleteProduct(${
                                      spGioHang.sanpham.id
                                    })" > <i class="fa-solid fa-xmark"></i> </button>
                                </td>
                            </tr>
                        `;
    contentHTML += trContent;
  }
  document.getElementById("tblGioHang").innerHTML = contentHTML;
};

let themSoLuong = (id) => {
  for (let i = 0; i < gioHang.length; i++) {
    if (+gioHang[i].sanpham.id === id) {
      gioHang[i].soluong += 1;
    }
  }
  renderCart(gioHang);
  tongTienThanhToan();
  saveCartInLocal();
};

let giamSoLuong = (id) => {
  for (let i = 0; i < gioHang.length; i++) {
    if (+gioHang[i].sanpham.id === id) {
      if (gioHang[i].soluong === 1) {
        gioHang.splice(i, 1);
        renderCart(gioHang);
        tongTienThanhToan();
        saveCartInLocal();
        return;
      }
      gioHang[i].soluong -= 1;
    }
  }
  renderCart(gioHang);
  tongTienThanhToan();
  saveCartInLocal();
};

let tongTienThanhToan = () => {
  let total = 0;
  let cost = 0;
  for (let i = 0; i < gioHang.length; i++) {
    cost = gioHang[i].sanpham.price * gioHang[i].soluong;
    total += cost;
  }
  document.getElementById("pay-money").innerHTML = `${total}đ`;
};

let thanhToan = () => {
  gioHang = [];
  saveCartInLocal();
  renderCart(gioHang);
  tongTienThanhToan();
};

let saveCartInLocal = () => {
  const jsonData = JSON.stringify(gioHang);
  console.log(jsonData);
  localStorage.setItem("carts", jsonData);
};

let getDataCart = () => {
  let dataJson = localStorage.getItem("carts");
  if (dataJson) {
    let data = JSON.parse(dataJson);
    for (let i = 0; i < data.length; i++) {
      let newProduct = new Product(data[i].sanpham, data[i].soluong);
      gioHang.push(newProduct);
    }
    renderCart(gioHang);
    console.log("Giỏ Hàng:", gioHang);
  }
};

let deleteProduct = (id) => {
  let foundIndex = findProductById(id);
  if (foundIndex === -1) {
    alert(" id không tồn tại");
    return;
  }
  gioHang.splice(foundIndex, 1);
  saveCartInLocal();
  renderCart(gioHang);
  tongTienThanhToan();
};

let findProductById = (id) => {
  for (let i = 0; i < gioHang.length; i++) {
    if (+gioHang[i].sanpham.id === id) {
      return i;
    }
  }
  return -1;
};

hienThiDanhSach();
getDataCart();
tongTienThanhToan();
