
export let layThongTinTuForm = () => {
    // let id = document.getElementById("foodID").value;
    let tenSP = document.getElementById("tenSP").value;
    let hangSX = document.getElementById("hangSX").value;
    let giaSP = document.getElementById("giaSP").value;
    let screenSP = document.getElementById("screenSP").value;
    let camFront = document.getElementById("camFront").value;
    let camBack = document.getElementById("camBack").value;
    let soLuong = document.getElementById("soLuong").value;
    let hinhSP = document.getElementById("hinhSP").value;
    let moTa = document.getElementById("moTa").value;

    return {tenSP, hangSX,giaSP,screenSP,camFront,camBack,soLuong,hinhSP,moTa};
}

export let showThongTin = (object)=>{
    // document.getElementById("spMa").innerText = object.ma;
    document.getElementById("spTenSP").innerText = object.name;
    document.getElementById("spHangSX").innerText = object.type;
    document.getElementById("spGia").innerText = object.price;
    document.getElementById("spScreen").innerText = object.screen;
    document.getElementById("spCamTruoc").innerText = object.frontCamera;
    document.getElementById("spCamSau").innerText = object.backCamera;
    document.getElementById("imgSP").src = object.img;
    document.getElementById("pMoTa").innerText = object.desc;
    document.getElementById("spSoLuong").innerText = object.quantity;
}